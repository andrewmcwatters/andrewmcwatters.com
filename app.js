/**
 * @license andrewmcwatters.com
 * (c) 2014 Andrew McWatters.
 */
'use strict';

var createServer = require('auto-sni');
var express      = require('express');
var compression  = require('compression');
var vhost        = require('vhost');
var cluster      = require('cluster');
var numCPUs      = require('os').cpus().length;

// disable for socket.io
var useCluster = false;
var i;

function sendWebapp(root) {
  return function (req, res) {
    res.sendFile(root + '/index.html');
  };
}

if (cluster.isMaster && useCluster) {
  // Fork workers.
  for (i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('online', function (worker) {
    console.log('worker ' + worker.process.pid + ' responded');
  });

  cluster.on('exit', function (worker) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {
  var app = express();
  module.exports = app;
  app.set('case sensitive routing', true);
  app.set('strict routing', true);

  if (cluster.isWorker) {
    app.use(function (req, res, next) {
      res.set('X-Worker-ID', cluster.worker.id);
      next();
    });
  }

  if (app.get('env') !== 'localhost') {
    app.use(function (req, res, next) {
      if (req.subdomains.length !== 0) {
        next();
        return;
      }

      var host = req.headers.host;
      if (host && host.indexOf('www.') !== 0) {
        res.redirect(req.protocol + '://www.' + host + req.originalUrl);
        return;
      }

      next();
    });
  }

  app.use(compression());

  createServer({
    email: 'mcwattersandrew@gmail.com',
    agreeTos: true,
    domains: [
      [
        'andrewmcwatters.com',
        'www.andrewmcwatters.com',
        'api.andrewmcwatters.com',
      ],
      [
        'planimeter.org',
        'www.planimeter.org',
        'api.planimeter.org',
      ],
    ],
  }, app);

  /**
   * API Server.
   */

  var api = require('../api.andrewmcwatters.com');
  app.use(vhost('api.andrewmcwatters.com', api));

  /**
   * Blog.
   */

  // var blog = require('../blog');
  // app.use(blog);

  /**
   * Planimeter.
   */

  app.use(vhost('*.planimeter.org', express.static(__dirname + '/planimeter')));

  app.use('/planimeter', function (req, res) {
    res.redirect('http://www.planimeter.org/');
  });

  var webapps = api.get('config').webapps;
  for (i = 0; i < webapps.length; i++) {
    var webapp  = webapps[i];
    var content = express.static(__dirname + '/' + webapp);
    app.use('/' + webapp, content);
    app.use('/' + webapp, sendWebapp(__dirname + '/' + webapp));
  }

  app.use(express.static(__dirname + '/public'));

  // AngularJS $location HTML5 mode
  app.use(function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
  });
}
