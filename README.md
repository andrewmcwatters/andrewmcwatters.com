# andrewmcwatters.com [![David](https://david-dm.org/andrewmcwatters/andrewmcwatters.com.svg)](https://david-dm.org/andrewmcwatters/andrewmcwatters.com) [![Codacy Badge](https://www.codacy.com/project/badge/c5b9e021d58b464591a0894bf6b79661)](https://www.codacy.com/app/andrewmcwatters/andrewmcwatters-com)

HTTP server for andrewmcwatters.com

Provisioning
============

As `root`, run `adduser --ingroup sudo andrewmcwatters`.

As `andrewmcwatters`, run the commands from install.sh, **manually.**
