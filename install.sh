#!/bin/sh
# UNDONE: Run with curl -sL https://raw.githubusercontent.com/andrewmcwatters/andrewmcwatters.com/master/install.sh | sudo -E bash -

# Update system.
sudo apt-get update
sudo apt-get upgrade -y

# Install packages.
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install git nodejs build-essential mongodb libcap2-bin python ruby ruby-dev libfontconfig -y
sudo setcap 'cap_net_bind_service=+ep' `which nodejs`

# Update NPM.
sudo npm install -g npm

# Fix NPM permissons.
# https://docs.npmjs.com/getting-started/fixing-npm-permissions

# Clone Express.js server and optional dependencies.
git config --global credential.helper store
git clone https://github.com/andrewmcwatters/andrewmcwatters.com.git
git clone https://github.com/andrewmcwatters/api.andrewmcwatters.com.git
git clone https://github.com/Planimeter/axis-api.git

# Install pm2.
npm install -g pm2

# Install node dependencies.
cd andrewmcwatters.com
npm install
cd ..
cd api.andrewmcwatters.com
npm install
cd ..
cd axis-api
npm install
cd ..

# Start server.
pm2 start andrewmcwatters.com/processes.json
pm2 save
sudo su -c "env PATH=$PATH:/usr/bin pm2 startup ubuntu -u andrewmcwatters --hp /home/andrewmcwatters"
